﻿using EquationSolver2._0.Enums;

namespace EquationSolver2._0.Domain
{
    public class Token
    {
        public TokenEnum TokenType { get; set; }
        public string Sequence { get; set; }
        public int Position { get; set; }

        public Token(TokenEnum tokenType, string sequence, int pos)
        {
            this.TokenType = tokenType;
            this.Sequence = sequence;
            this.Position = pos;
        }
    }
}