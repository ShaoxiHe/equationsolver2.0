﻿using System.Collections.Generic;

namespace EquationSolver2._0.Domain
{
    public class Equation
    {
        public LinkedList<Token> LeftTokens { get; set; }
        public LinkedList<Token> RightTokens { get; set; }
    }
}