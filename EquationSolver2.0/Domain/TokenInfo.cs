﻿using System.Text.RegularExpressions;
using EquationSolver2._0.Enums;

namespace EquationSolver2._0.Domain
{
    public class TokenInfo
    {
        public Regex Rgx { get; set; }
        public TokenEnum TokenType { get; set; }

        public TokenInfo()
        {
        }

        public TokenInfo(Regex rgx, TokenEnum tokenType)
        {
            this.Rgx = rgx;
            this.TokenType = tokenType;
        }
    }
}