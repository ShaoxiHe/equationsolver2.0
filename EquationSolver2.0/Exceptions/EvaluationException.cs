﻿using System;

namespace EquationSolver2._0.Exceptions
{
    public class EvaluationException : Exception
    {
        public EvaluationException(string message) : base(message)
        {
            
        }
    }
}