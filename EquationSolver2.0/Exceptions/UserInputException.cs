﻿using System;

namespace EquationSolver2._0.Exceptions
{
    public class UserInputException : Exception
    {
        public UserInputException()
        {

        }

        public UserInputException(string message) : base(message)
        {
            
        }
    }
}