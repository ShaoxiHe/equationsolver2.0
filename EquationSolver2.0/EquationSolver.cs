﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Principal;
using EquationSolver2._0.Domain;
using EquationSolver2._0.Enums;
using EquationSolver2._0.Exceptions;
using EquationSolver2._0.Utillities;

namespace EquationSolver2._0
{
    public class EquationSolver
    {
        // TODO: validate the quation here

        public static void Solve(string equationStr)
        {
            Console.WriteLine("Trying to solve \"{0}\"...", equationStr);

            // Get tokens
            Tokenizer tokenizer = Tokenizer.GetTokenizer();
            LinkedList<Token> equationTokens = tokenizer.tokenize(equationStr);

            
            LinkedList<Token> leftTokensInfix = new LinkedList<Token>();
            LinkedList<Token> rightTokensInfix = new LinkedList<Token>();
            bool left = true;

            // 取出左右中缀表达式
            while (equationTokens.Count > 0)
            {
                LinkedListNode<Token> lookAheadNode = equationTokens.First;

                if (lookAheadNode.Value.TokenType != TokenEnum.EqualSign)
                {
                    if (left)
                    {
                        leftTokensInfix.AddLast(lookAheadNode.Value);
                    }
                    else
                    {
                        rightTokensInfix.AddLast(lookAheadNode.Value);
                    }
                }
                else
                {
                    left = false;
                }

                equationTokens.RemoveFirst();
            }

            // 分别成后缀表达式
            LinkedList<Token> leftTokensPostfix = getPostfixTokens(leftTokensInfix);
            foreach (Token token in leftTokensPostfix)
            {
                Console.Write(token.Sequence);
            }
            Console.WriteLine();

            LinkedList<Token> rightTokensPostfix = getPostfixTokens(rightTokensInfix);
            foreach (Token token in rightTokensPostfix)
            {
                Console.Write(token.Sequence);
            }
        }

        private static LinkedList<Token> getPostfixTokens(LinkedList<Token> tokensInfix)
        {
            LinkedList<Token> tokensPostfix = new LinkedList<Token>();

            // Can create a class to hold operations. and overwrite operations
            Stack<Token> operationStack = new Stack<Token>();

            while (tokensInfix.Count > 0)
            {
                Token lookAhead = tokensInfix.First.Value;
                LinkedListNode<Token> lookAheadNode = tokensInfix.First;
                switch (lookAhead.TokenType)
                {
                    case TokenEnum.Number:
                        tokensPostfix.AddLast(lookAhead);
                        break;
                    case TokenEnum.Raised:
                        operationStack.Push(lookAhead);
                        break;
                    case TokenEnum.Multdiv:
                        if (operationStack.Count == 0 || operationStack.Peek().TokenType == TokenEnum.Plusminus)
                        {
                            operationStack.Push(lookAhead);
                        }
                        else
                        {
                            while (operationStack.Count > 0)
                            {
                                tokensPostfix.AddLast(operationStack.Pop());
                            }
                            operationStack.Push(lookAhead);
                        }
                        break;
                    case TokenEnum.Plusminus:
                        if (operationStack.Count == 0)
                        {
                            operationStack.Push(lookAhead);
                        }
                        else if (operationStack.Peek().TokenType == TokenEnum.Multdiv || operationStack.Peek().TokenType == TokenEnum.Raised)
                        {
                            while (operationStack.Count > 0)
                            {
                                tokensPostfix.AddLast(operationStack.Pop());
                            }
                            operationStack.Push(lookAhead);
                        }
                        break;
                    default:
                        throw new EvaluationException("What?");
                }
                tokensInfix.RemoveFirst();
                if (tokensInfix.Count != 0) continue;
                while (operationStack.Count > 0)
                {
                    tokensPostfix.AddLast(operationStack.Pop());
                }
            }

            return tokensPostfix;
        }
    }
}