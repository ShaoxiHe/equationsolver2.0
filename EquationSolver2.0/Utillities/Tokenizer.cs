﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using EquationSolver2._0.Domain;
using EquationSolver2._0.Enums;
using EquationSolver2._0.Exceptions;

namespace EquationSolver2._0.Utillities
{
    public class Tokenizer
    {
        private static Tokenizer _tokenizer = null;

        public LinkedList<TokenInfo> TokenInfos { get; set; }

        public Tokenizer()
        {
            TokenInfos = new LinkedList<TokenInfo>();
        }

        public static Tokenizer GetTokenizer()
        {
            return _tokenizer ?? (_tokenizer = createTokenizer());
        }

        private static Tokenizer createTokenizer()
        {
            Tokenizer tokenizer = new Tokenizer();

            tokenizer.add("[+-]", TokenEnum.Plusminus);
            tokenizer.add("[*/]", TokenEnum.Multdiv);
            tokenizer.add("\\^", TokenEnum.Raised);

            tokenizer.add("\\(", TokenEnum.OpenBracket);
            tokenizer.add("\\)", TokenEnum.CloseBracket);
            tokenizer.add("(?:\\d+\\.?|\\.\\d)\\d*(?:[Ee][-+]?\\d+)?", TokenEnum.Number);
            tokenizer.add("[a-zA-Z]\\w*", TokenEnum.Variable);
            tokenizer.add("[=]", TokenEnum.EqualSign);

            return tokenizer;
        }

        private void add(string regex, TokenEnum tokenType)
        {
            TokenInfos.AddLast(new TokenInfo(new Regex("^(" + regex+")"), tokenType));
        }

        public LinkedList<Token> tokenize(string equation)
        {
            int totalLength = equation.Length;
            LinkedList<Token> tokens = new LinkedList<Token>();
            tokens.Clear();

            while (!equation.Equals(""))
            {
                int remaining = equation.Length;
                bool hasMatch = false;

                foreach (TokenInfo tokenInfo in TokenInfos)
                {
                    Match match = tokenInfo.Rgx.Match(equation);

                    if (!match.Success) continue;
                    
                    hasMatch = true;
                    string tok = match.Value;

                    equation = tokenInfo.Rgx.Replace(equation, "");
                    tokens.AddLast(new Token(tokenInfo.TokenType, tok, totalLength-remaining));
                    break;
                }
                
                if (!hasMatch)
                    throw new UserInputException("Unexpected character in input: " + equation);
            }

            return tokens;
        }
    }
}