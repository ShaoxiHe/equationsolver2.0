﻿using System;
using System.Linq;
using EquationSolver2._0.Exceptions;

namespace EquationSolver2._0
{
    public class Calculator
    {
        public Calculator()
        {
        }

        public Calculator(string[] args)
        {
            try
            {
                if (args.Length >= 2)
                {
                    if (args[0].Equals("calc"))
                    {
                        // Extract the equation from the array of arguments
                        EquationSolver.Solve(ExtractEquationFromArguments(args));
                    }
                }
                else
                {
                    // print helper
                    Console.WriteLine("Print helper.");

                    // and accept more commands
                }
            }
            catch (Exception e)
            {
                if (e is UserInputException || e is EvaluationException)
                {
                    Console.WriteLine("An error has occurred: {0}", e.Message);
                }
                else
                {
                    Console.WriteLine("Unknown error has occurred. {0}", e);
                }
            }
            
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

        private string ExtractEquationFromArguments(string[] args)
        {
            return args.Where((t, i) => i != 0).Aggregate("", (current, t) => current + t);
        }
    }
}