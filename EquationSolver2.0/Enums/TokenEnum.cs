﻿namespace EquationSolver2._0.Enums
{
    public enum TokenEnum
    {
        Epsilon,
        Plusminus,
        Multdiv,
        Raised,
        OpenBracket,
        CloseBracket,
        Number,
        Variable,
        EqualSign
    }
}